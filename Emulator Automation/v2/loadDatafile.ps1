# Set the local script path
$SCRIPTPATH = split-path -parent $MyInvocation.MyCommand.Definition

Write-Host "========================================"
$EMU_HOST = Read-Host -Prompt 'Enter the hostname or IP address for the emulator'
Write-Host "========================================"
$TESTCASE = Read-Host -Prompt 'Enter the test case ID'
Write-Host "========================================"

# Read in the xml file that contains the fps.xml config for each test case
[xml]$EMULATORCONFIG = Get-Content configuration.xml

foreach( $CONFIG in $EMULATORCONFIG.testcases.testcase) {
    $TESTID = $CONFIG.id    
    If ($TESTID -eq $TESTCASE) {
        $EMULATOR = $CONFIG.emulator
        $DATAFILE = $CONFIG.datafile
        $FOUND = 1
    } 
}

# If the test case wasn't found in the config file, exit.
If (-Not $FOUND) {
    Write-Host "ERROR: Could not find the specified test case in the configuration file. Exiting..."
    exit
} 

# Transfer the datafile to the emulation environment via SCP
$FILEPATH = "$SCRIPTPATH\data\$DATAFILE"
Write-Host "Transferring $DATAFILE to the emulation environment..."
c:\progra~2\putty\pscp.exe -pw ibycgtpw $FILEPATH root@${EMU_HOST}:/opt/emulation/data
Write-Host "Transferred."
Write-Host "========================================"

Write-Host "Adding entry to the fps.xml file..."

$FPSPATH = '/opt/emulation/vms/$HOSTNAME'
$FPSSCRIPT = @"
#!/bin/bash

if grep -q "{0}" {1}/fps.xml; then
    sed 's/<item name=\"{0}\".*/<item name=\"{0}\" value=\"--data \/opt\/emulation\/data\/{2}\" \/>/g' {1}/fps.xml > {1}/fps.xml.new
    mv {1}/fps.xml.new {1}/fps.xml
else
    sed '/<\/config>/ i\
    <item name=\"{0}\" value=\"--data \/opt\/emulation\/data\/{2}\" \/>' {1}/fps.xml > {1}/fps.xml.new
    mv {1}/fps.xml.new {1}/fps.xml
fi

service emulators stop
service emulators start
"@ -f $EMULATOR, $FPSPATH, $DATAFILE

Set-Content -Value $FPSSCRIPT -Path c:\Temp\configurefps.sh

c:\progra~2\putty\pscp.exe -pw ibycgtpw c:\Temp\configurefps.sh root@${EMU_HOST}:/tmp

$EMUCOMMANDS = @"
sed $'s/\r//' /tmp/configurefps.sh > /tmp/configurefpsfixed.sh
mv /tmp/configurefpsfixed.sh /tmp/configurefps.sh
chmod 755 /tmp/configurefps.sh
/tmp/configurefps.sh
rm -f /tmp/configurefps.sh
"@

Set-Content -Value $EMUCOMMANDS -Path c:\Temp\runconfigfsp.txt

c:\progra~2\putty\plink.exe -pw ibycgtpw root@${EMU_HOST} -m c:\Temp\runconfigfsp.txt

Remove-Item c:\Temp\configurefps.sh
Remove-Item c:\Temp\runconfigfsp.txt

Write-Host "All done."

