Write-Host "========================================"
$EMU_HOST = Read-Host -Prompt 'Enter the hostname or IP address for the emulator'
Write-Host "========================================"
Write-Host "Use a release build or private build?"
Write-Host "1. Release build"
Write-Host "2. Private build"
$BUILD_TYPE = Read-Host -Prompt 'Enter your choice'

$BUILD_NAME = Read-Host -Prompt 'Enter the build name (i.e. 2012.44)'

If($BUILD_TYPE -eq "1") {
	$BASE = "\\bob\builds\STREAMS FP\Release\RH73"

	$BUILD_PATHS = @()

	Get-ChildItem -Path $BASE\$BUILD_NAME | foreach -Begin {$i=0} -Process {
	$i++
	$BUILD_PATHS += $_.FullName
	"{0}. {1}" -f $i,$_.Name
	} -outvariable menu

	$r = Read-Host "Select a build number"
	$BUILD_PATH = $BUILD_PATHS[$r-1]

	Write-Host "========================================"

	$PACKAGE = Get-ChildItem $BUILD_PATH\streams-*.img
	Write-Host "Using Package: $PACKAGE"

	Write-Host "========================================"

	$USE_CURRENT = Read-Host "Use current for emulator image? (y/n)"

	If($USE_CURRENT -eq "y") {

		Write-Host "========================================"

		$EMU_PATH = "\\bob\builds\STREAMS FP\current"
		$LATEST = Get-ChildItem -Path $EMU_PATH | sort LastWriteTime | select -last 1
		$EMULATORS = Get-ChildItem $EMU_PATH\$LATEST\emulators-*.img
		Write-Host "Using Emulators: $EMULATORS"

		Write-Host "========================================"
	}

	If($USE_CURRENT -eq "n") {
		$EMULATORS = Read-Host "Enter the full path for the emulators image file"
	}
}

If($BUILD_TYPE -eq "2") {
	$BASE = "\\bob\builds\STREAMS FP\Branches"
	Write-Host "Searching for the build in $BASE"
	$BUILD_PATHS = @()

	Get-ChildItem -Path $BASE\$BUILD_NAME -Filter $PACKAGE -Recurse | foreach -Begin {$i=0} -Process {
	$i++
	$BUILD_PATHS += $_.FullName
	"{0}. {1}" -f $i,$_.FullName
	} -outvariable menu

	$r = Read-Host "Select the correct build from the list"
	$BUILD_PATH = $BUILD_PATHS[$r-1]

	Write-Host "========================================"

	$BUILD_NUMS = @()
	$BUILD_NUMS_PATH = @()

	Get-ChildItem -Path $BUILD_PATH | foreach -Begin {$i=0} -Process {
	$i++
	$BUILD_NUMS += $_.Name
	$BUILD_NUMS_PATH += $_.FullName
	"{0}. {1}" -f $i,$_.Name
	} -outvariable menu

	$r = Read-Host "Select a build"

	Write-Host "========================================"

	$BUILD_PATH = $BUILD_NUMS_PATH[$r-1]
	$PACKAGE = Get-ChildItem $BUILD_PATH\streams-*.img
	Write-Host "Using Package: $PACKAGE"
	$EMULATORS = Get-ChildItem $BUILD_PATH\emulators-*.img
	Write-Host "Using Emulators: $EMULATORS"

	Write-Host "========================================"
}

Write-Host "Transferring images to the emulation environment..."

c:\progra~2\putty\pscp.exe -pw ibycgtpw $PACKAGE root@${EMU_HOST}:/tmp
c:\progra~2\putty\pscp.exe -pw ibycgtpw $EMULATORS root@${EMU_HOST}:/tmp

Write-Host "========================================"

Write-Host "Setting symbolic links on the emulation environment..."


$PACKAGE_NAME = Split-Path $PACKAGE -leaf
$CP = "mv /tmp/$PACKAGE_NAME" + ' /opt/emulation/vms/$HOSTNAME'
Set-Content -Value $CP -Path c:\Temp\devfpxcommands.txt
$PACKAGE_LINK = 'ln -sf /opt/emulation/vms/$HOSTNAME/' + $PACKAGE_NAME + ' /opt/emulation/vms/$HOSTNAME/package'
Add-Content -Value $PACKAGE_LINK c:\Temp\devfpxcommands.txt

$EMULATORS_NAME = Split-Path $EMULATORS -leaf
$CP = "mv /tmp/$EMULATORS_NAME" + ' /opt/emulation/vms/$HOSTNAME'
Add-Content -Value $CP -Path c:\Temp\devfpxcommands.txt
$EMULATORS_LINK = 'ln -sf /opt/emulation/vms/$HOSTNAME/' + $EMULATORS_NAME + ' /opt/emulation/vms/$HOSTNAME/emulators'
Add-Content -Value $EMULATORS_LINK c:\Temp\devfpxcommands.txt

Add-Content -Value "service emulators stop" -Path c:\Temp\devfpxcommands.txt
Add-Content -Value "service emulators start" -Path c:\Temp\devfpxcommands.txt

Add-Content -Value "killall stunnel" -Path c:\Temp\devfpxcommands.txt
Add-Content -Value "service tunnel stop" -Path c:\Temp\devfpxcommands.txt
Add-Content -Value "service tunnel start" -Path c:\Temp\devfpxcommands.txt

c:\progra~2\putty\plink.exe -pw ibycgtpw root@${EMU_HOST} -m c:\Temp\devfpxcommands.txt

Remove-Item c:\Temp\devfpxcommands.txt

Write-Host "All done."

