import socket
import sys
from thread import *

HOST = ''
FOWARD_HOST = '192.168.100.1'
PORT = 20000 # Arbitrary non-privileged port

tcpHost = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpForward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Sockets created'

#Bind socket to local host and port
try:
    tcpHost.bind((HOST, PORT))
    tcpForward.connect((FOWARD_HOST, PORT))
except:
    print 'Bind failed.'
    sys.exit()

print 'Socket bind complete'

#Start listening on socket
tcpHost.listen(10)
print 'Socket now listening'

#Function for handling connections. This will be used to create threads
def clientthread(conn):

    #infinite loop so that functions do not terminate and thread does not end.
    while True:

        #Receiving from client
        data = conn.recv(1024)
        if not data:
            break
        data = data.strip()
        print '===='
        print 'received: ' + data
        print '===='
        tcpForward.send(data)

    #came out of loop
    conn.close()

#now keep talking with the client
while 1:
    #wait to accept a connection - blocking call
    conn, addr = tcpHost.accept()
    print 'Connected with ' + addr[0] + ':' + str(addr[1])

    start_new_thread(clientthread ,(conn,))

tcpHost.close()
tcpForward.close()
