import socket
import serial
import sys
from thread import *

HOST = ''   # Symbolic name meaning all available interfaces
PORT = 4000 # Arbitrary non-privileged port

s = None
global s
s = serial.Serial("/dev/ttyS0", 57600)

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'

#Bind socket to local host and port
try:
    tcp.bind((HOST, PORT))
except:
    print 'Bind failed.'
    sys.exit()

print 'Socket bind complete'

#Start listening on socket
tcp.listen(10)
print 'Socket now listening'

#Function for handling connections. This will be used to create threads
def clientthread(conn):

    #infinite loop so that functions do not terminate and thread does not end.
    while True:

        #Receiving from client
        data = conn.recv(1024)
        if not data:
            break
        print '===='
        print 'received: ' + data
        print '===='
        s.write(data)

    #came out of loop
    conn.close()

#now keep talking with the client
while 1:
    #wait to accept a connection - blocking call
    conn, addr = tcp.accept()
    print 'Connected with ' + addr[0] + ':' + str(addr[1])

    start_new_thread(clientthread ,(conn,))

tcp.close()
