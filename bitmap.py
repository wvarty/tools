import pygame
import math
import socket
import sys
from thread import *

######## SETUP PYGAME ########

pygame.display.init()
window = pygame.display.set_mode((512, 230))
signImg = pygame.image.load("sign.bmp")

######## SETUP TCP SERVER ########

HOST = ''   # Symbolic name meaning all available interfaces
PORT = 27001 # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setblocking(False)
print 'Socket created'

#Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except:
    print 'Bind failed.'
    sys.exit()
print 'Socket bind complete'

#Start listening on socket
s.listen(5)
print 'Socket now listening'

done = False

while not done:
    try:
        conn, addr = s.accept()
        print 'Connected with ' + addr[0] + ':' + str(addr[1])
        data = conn.recv(4096)
        conn.close()

        if data:
            signImg = pygame.image.fromstring(data)
        else:
            print 'Failed to receive bitmap data'

    except socket.error, e:
        print e

    window.fill((0,0,0))
    evtList = pygame.event.get()
    for evt in evtList:
       if evt.type == pygame.QUIT:
          done = True

    window.blit(signImg, (0,0))
    pygame.display.update()

s.close()
pygame.quit()